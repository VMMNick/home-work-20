function filterCollection(collection, keywords, allKeywordsRequired, ...fields) {
    // Розбиваємо ключові слова на окремі слова
    const keywordList = keywords.toLowerCase().split(' ');
  
    // Функція для рекурсивного пошуку ключових слів у властивостях об'єкта
    function searchInObject(obj, keywords, allKeywordsRequired) {
      for (const field of fields) {
        const value = getFieldByPath(obj, field);
        if (typeof value === 'string') {
          const lowerValue = value.toLowerCase();
          const foundKeywords = keywordList.filter(keyword => lowerValue.includes(keyword));
          if (allKeywordsRequired && foundKeywords.length !== keywordList.length) {
            return false;
          }
          if (!allKeywordsRequired && foundKeywords.length > 0) {
            return true;
          }
        }
        if (Array.isArray(value)) {
          for (const item of value) {
            if (searchInObject(item, keywords, allKeywordsRequired)) {
              return true;
            }
          }
        }
        if (typeof value === 'object' && value !== null) {
          if (searchInObject(value, keywords, allKeywordsRequired)) {
            return true;
          }
        }
      }
      return false;
    }
  
    // Функція для отримання значення поля об'єкта за шляхом
    function getFieldByPath(obj, path) {
      const keys = path.split('.');
      let value = obj;
      for (const key of keys) {
        if (value.hasOwnProperty(key)) {
          value = value[key];
        } else {
          return undefined;
        }
      }
      return value;
    }
  
    // Фільтруємо колекцію
    const filteredCollection = collection.filter(item => searchInObject(item, keywords, allKeywordsRequired));
  
    return filteredCollection;
  }
  
  // Приклад використання функції
  const vehicles = [
    {
      name: 'Toyota',
      description: 'Some description',
      contentType: {
        name: 'Car',
      },
      locales: [
        { name: 'en_US', description: 'English' },
        { name: 'fr_FR', description: 'French' },
      ],
    },
    // Додайте інші об'єкти в колекцію
  ];
  
  const filteredVehicles = filterCollection(vehicles, 'en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description');
  console.log(filteredVehicles);
